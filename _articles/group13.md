---
layout: Application mobilité Genève
title: Groupe 13
autotoc: true
---

### La situation actuelle en Suisse (contexte)

Il existe actuellement des applications différentes pour se déplacer, mais aucune qui regroupe tous les moyens de transports tels que les transports publics (bus, tram, bateau, train), la mobilité partagée (covoiturage, location de vélo, location de trottinette, location de voiture partagée), l'avion, Taxi, Uber.
Application existante : Google maps, TPG, Uber, compagnie aérienne, SFF

#### Analyse de sous-systèmes en Suisse

Selon la Tribune de Genève, une enquête a été réalisée par Andrea Baranzini disant que la mobilité est la pire bête noire du Grand Genève en démontrant que les déplacements à Genève sont de plus en plus compliqués pour les citoyens et ils en sont de moins en moins contents. 

#### La situation internationale

En France, c'est le même principe qu'en Suisse : plusieurs applications existent pour tout séparer et c'est également le cas dans le monde. 

### L' Application de la culture computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Notre défis (description générique)

→ Le but est de pouvoir faciliter la recherche de transports (mobilité partagée, transports publics) pour se déplacer d’un point A à un point B à travers la Suisse. 

→ Seulement 1,5% des citoyens suisses louent un vélo pour se déplacer, cela montre que peu de personnes ont accès à cette information. 

→ De manière générale, la population suisse privilégie la voiture, les transports publics ainsi que les vélos personnels car elle ignore que d’autres moyens de locomotion sont mis à disposition. 


#### Abstraction (ce qu'on cherche à résoudre)

 On cherche à centraliser l’information concernant les moyens de mobilités alternatifs tels que la bicyclette et les trottinettes électriques ainsi que le covoiturage et/ou la location de voiture à courte durée pour combler à la sous-exploitation de ces moyens. → favoriser la recherche de transport 

#### Décomposition du problème

 Facilité de trouver plusieurs moyens de transport selon le prix. 

##### Les utilisateurs

Toutes personnes à Genève se déplacent aujourd'hui alors tout le monde peut avoir envie d'accéder à l'information.

##### Procédures principales

Pour le covoiturage à Genève, il n'existe pas d'application mais on pourrait imaginer que les personnes voulant faire du covoiturage peuvent s'inscrire dans un système en indiquant leur trajet et le prix. 

##### Organisations concernés

Les applications concernées seraient Tpg, Taxi, Uber, mobility, SFF, service de covoiturage, service de location de trottinettes et vélos. 

##### Systèmes disponibles

TPG, SFF, Taxi et Uber. 

##### Les données concernées (type)

TPG, SBB : horaires, arrêt , prix, fréquentations 
Taxi, Uber : chauffeur disponible selon l'endroit, prix

### L' Application de la pensée computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les patterns à comprendre et réutiliser

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les scenarii

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### "L'algorithme" qui en découle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### Sources

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.
