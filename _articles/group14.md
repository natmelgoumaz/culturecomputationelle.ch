---
layout: activity
title: Guide insolite
autotoc: true
---

### La situation actuelle en Suisse (contexte)

Le tourisme est un secteur important en Suisse. X personnes visitent le pays chaque année. Selon les chiffres 2019 publié par l'OFS en 2020, les premiers touristes sont les Suisses eux-mêmes, en deuxième position nous retrouvons les Européens, puis le continent asiatique, américain et enfin l'Afrique et l'Océanie.
Afin de répondre aux besoins des touristes, les offices de tourisme et autres organismes produisent divers contenu à travers des guides papier, des sites internet et des applications. Ces canaux de communication permettent aux touristes de trouver notamment des informations générales sur la Suisse, planififer des visites et ...

#### Analyse de sous-systèmes en Suisse

 Nous pouvons observer que chaque canton effectue la promotion de sa région à travers des offices de tourisme mais également à l'aide d'application spécifique à la ville ou la région.

#### La situation internationale

Dans les pays voisins, de nombreux sites web tel que TripAdvisor et applications mobiles sont destiné à l'usage des touristes. Les informations disponibles sur ce site web sont de plusieurs nature. Les touristes peuvent notamment trouver et réserver des guides, avoir des idées d'activités dans une ville, ou encore trouver des hébergements et restaurants.

### L' Application de la culture computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Notre défis (description générique)

Actuellement nous constatons que chaque canton promouvoit le tourisme dans sa région et qu'il y a peu de coordination. Les informations destinées aux touristes sont générales et regroupent des propositions plus ou moins similaires, avec pour conséquent un tourisme de masse. A l'heure du tourisme insolite et expérientiel, de nouvelles stratégies doivent être développées.

#### Abstraction (ce qu'on cherche à résoudre)

Nous souhaitons mutualiser l'échange d'expériences entre touristes à travers une application collaborative afin de favoriser et développer le tourisme insolite en Suisse romande. Le but est que chaque personne puisse découvrir un nouveau lieu, tel un local.

#### Décomposition du problème

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les utilisateurs

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Procédures principales

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Organisations concernés

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Systèmes disponibles

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les données concernées (type)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### L' Application de la pensée computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les patterns à comprendre et réutiliser

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les scenarii

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### "L'algorithme" qui en découle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### Sources

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.
