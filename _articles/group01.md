---
layout: activity
title: Les métiers de la comptabilité et les robots
autotoc: true
---

### La situation actuelle en Suisse (contexte)

- Nombre d'emplois dans le domaine
- Prévisions futurs (évolution statistiques)
- Digitalisation des métiers de manière globale et en comptabilité
- Evolution des métiers et nombre de comptable
- Evolution des utilisations des programmes classiques (également impact covid)
- Niveau de salaire
- Formation proposées au sein des fiduciaires
- Impact de l'AI sur le secteur comptable 
- description 
-
-




 Test de 02.11.2021

#### Analyse de sous-systèmes en Suisse

Différents métiers de la comptabilité
Analyse sur chacun de ces métiers & situation

#### La situation internationale

- Nombre d'emplois dans le domaine
-  Prévisions futurs (évolution statistiques)
-  Digitalisation des métiers de manière globale et en comptabilité

### L' Application de la culture computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Notre défis (description générique)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Abstraction (ce qu'on cherche à résoudre)

Problématique : De quelle manière les métiers de la comptabilité peuvent-ils être remplacé par des robots ?

#### Décomposition du problème

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les utilisateurs

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Procédures principales

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Organisations concernés

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Systèmes disponibles

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

##### Les données concernées (type)

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### L' Application de la pensée computationnelle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les patterns à comprendre et réutiliser

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### Les scenarios

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

#### "L'algorithme" qui en découle

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.

### Sources

 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 tempor incididunt ut labore et dolore magna aliqua.
